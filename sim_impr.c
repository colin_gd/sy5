#include "prlib.h"

void usage();

int
main(int argc, char **argv)
{
	/* Simulateur d'imprimante qui lit de façon bloquante dans un tube puis écris dans /dev/null, puis
	s'endore trois secondes par page, de 2048 octets. Si à la fin du message il y dix caractère nulles,
	le simulateur s'endort deux secondes en attendant des fichiers. */
	
	
	
	/***************
	* DECLARATIONS *
	***************/
	
	char *pfifo, *pname, *cur, *zero, buf[MAX_CANON];
	int ch, fdes, null, r, stime = 0, total = 0, stop = 0;
	
	/*******************
	* FIN DECLARATIONS *
	*******************/
	
	
	
	/**********************
	* GESTION D'ARGUMENTS *
	**********************/
	
	if (argc != 5)
		usage();
	
	while ((ch = getopt( argc, argv, "+t:n:")) != -1)
		switch ( ch ){
			case 't' :
				pfifo = optarg;
				break;
			case 'n' :
				pname = optarg;
				break;
			default:
			case '?' :
				usage();
				break;
		}
	
	/**************
	* FIN GESTION *
	**************/
	
	
	
	/**********************
	* OUVERTURE BLOQUANTE *
	**********************/
	
	if ((mkfifo(pfifo,S_IRUSR|S_IWUSR) < 0) && (errno != EEXIST))
		error("mkfifo: ");
	
	if ((fdes = open(pfifo, O_RDONLY)) == -1)
		error("opend: ");
	
	if ((null = open("/dev/null", O_WRONLY)) == -1)
		error("open: ");
	
	/****************
	* FIN OUVERTURE *
	****************/
	
	
	
	/****************************************************
	* LECTURE DANS LE TUBE PUIS ECRITURE DANS /dev/null *
	****************************************************/
	
	for (;;){
		if ((r = read(fdes, buf, MAX_CANON)) > 0){
			cur = buf;
			while (cur < (buf + r)){
				if ( (zero = bufchr(cur, '\0', r)) != NULL){
					do{
						if ( *(zero) == '\0'){
							stop++;
							zero++;
						}else{
							stop = 0;
							zero++;
							break;
						}
					}while (stop != 10 && zero < (buf + r));
					write (null, cur, zero - cur - 1);
					total += zero - cur;
					cur = zero;
					zero = NULL;
				}else{
					write(null, buf, r);
					total += r;
					stop = 0;
					break;
				}
				if (stop == 10){
					stime = (total / BYTES_PAGE + 1) * 3;
					total = 0;
					stop = 0;
					printf("%s: Printing, wait %d seconds.\n", pname, stime);
					sleep(stime);
				}
			}
		}else if (r == 0){
			sleep(2);
		}else if (r < 0 && errno != EAGAIN)
			error("read: ");
	}
	/***********************
	* FIN LECTURE ECRITURE *
	***********************/
	
	return 0;
}

void
usage()
{
	/* Fonction pour afficher un message d'erreur lors de la mauvaise utilisation de la commande. */
	
	err("usage: sim_impr -t tube_in -n name");
}
