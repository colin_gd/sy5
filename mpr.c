#include "prlib.h"

void usage();

int
main(int argc, char **argv)
{
	/* Client d'impression, verification du nombre d'arguments, parsage des arguments,
	verification du type de fichier, creation du message, ouverture du tube, ecriture dans le tube,
	fin du programme. Retourne 0 si aucune erreur de s'est porduit, 1 sinon. */
	
	
	
	/***************
	* DECLARATIONS *
	***************/

	struct stat sbuf;
	int fdes, ch;
	char *fserv, *fname, ref_abs[PATH_MAX+1];
	msg_t msg;
	
	/*******************
	* FIN DECLARATIONS *
	*******************/
	
	
	
	/**********************
	* GESTION D'ARGUMENTS *
	**********************/
	
	if (argc != 4)
		usage();
	
	while ((ch = getopt( argc, argv, "+P:")) != -1)
		switch ( ch ){
			case 'P' :
				msg.imprimante = optarg;
				break;
			default:
			case '?' :
				usage();
				break;
		}
	argv += optind;
	fname = argv[0];
	
	/**************
	* FIN GESTION *
	**************/
	
	
	
	/***********************
	* VERIFICATION FICHIER *
	************************/
	
	if (lstat(fname, &sbuf) != 0)
		error(fname);
	
	if (S_ISREG(sbuf.st_mode) == 0){
		fprintf(stderr, "%s: Not a regular file.\n", fname);
		exit(EXIT_FAILURE);
	}
	
	/*******************
	* FIN VERIFICATION *
	********************/
	
	
	
	/*************************
	* COMPOSITION DU MESSAGE *
	*************************/
	
	if ((msg.ref_abs = abs_path(fname, ref_abs))==NULL)
		error("abs_path: ");
	msg.type = 'i';
	msg.gid = getgid();
	msg.uid = getuid();
	
	msg.lng = sizeof(char) + sizeof(uid_t) + sizeof(gid_t) + strlen(msg.imprimante) + strlen(msg.ref_abs) + 2;
	
	/**************
	* FIN MESSAGE *
	**************/
	
	
	
	/*******************
	* ECRITURE MESSAGE *
	*******************/
	
	if ((fserv = getenv("IMP_PATH")) == NULL)
		error("IMP_PATH not defined.\n");
	
	if ((fdes = open(fserv, O_WRONLY)) == -1){
		fprintf(stderr, "Cannot communicate with server.\n");
		exit(EXIT_FAILURE);
	}
	
	if(write_msg(fdes, msg) < 1)
	  error("write msg: ");
	
	/***************
	* FIN ECRITURE *
	***************/
	
	exit(EXIT_SUCCESS);
}

void
usage()
{
	/* Fonction pour afficher un message d'erreur lors de la mauvaise utilisation de la commande. */
	
	err("usage: mpr -P printer file");
}
