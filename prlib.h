#ifndef _PRLIB_H_
#define _PRLIB_H_

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include "fifo.h"

#ifndef PATH_MAX
#define PATH_MAX 1024
#endif /* PATH MAX */

#ifndef MAX_CANON
#define MAX_CANON 1024
#endif /* MAX_CANON */

#ifndef LINE_MAX
#define LINE_MAX 128
#endif /* LINE_MAX */

#define BYTES_PAGE 2048

#define SIM_CMD 6 /* MAX size of sim_impr command */

#define PR_MAX 64

/***********************
* STRUCTURE DU MESSAGE *
***********************/

typedef struct msg_s{
	unsigned int lng;
	char type;
	uid_t uid;
	gid_t gid;
	char *imprimante;
	char *ref_abs;
} msg_t;

/****************
* FIN STRUCTURE *
****************/



/***********************
* STRUCTURE IMPRIMANTE *
***********************/

typedef struct pr_s
{
  fifo_t *fifo;
  char *name;
  int td_pr; /* tube descriptor printer */
  int fdes; /* current printing file descriptor */
} pr_t;

/****************
* FIN STRUCTURE *
****************/



/******************
* ERROR HANDLING *
******************/

void error(const char *);
void err(const char *);
void warning(const char *);
void warn(const char *);

/**********************
* FIN ERROR HANDLING *
**********************/



/*******************
* MESSAGE HANDLING *
*******************/

int read_msg(int ,msg_t *);
int write_msg(int ,msg_t);
void printm(msg_t);

/**************
* FIN MESSAGE *
**************/




/***************
* PR HANDLING *
***************/

pr_t *new_pr(char *);

void free_pr(pr_t *);

/*********
* FIN PR *
*********/



/****************
* PATH HANDLING *
****************/

char *abs_path(char *, char *);

/***********
* FIN PATH *
***********/



/**************
* LINE PARSER *
**************/

int getline(int, char *);

/******************
* FIN LINE PARSER *
******************/



/************
* BUF TOOLS *
************/

char *bufchr(const char *, char, size_t);

void smakea(char *, char *, char **);

int read_conf(char *, pr_t **);

/****************
* FIN BUF TOOLS *
****************/



/***********************
* PERMISSIONS HANDLING *
***********************/

int is_readable(char *, uid_t, gid_t);

/*******************
* FIN PERSMISSIONS *
*******************/



#endif /* _PRLIB_H_ */
