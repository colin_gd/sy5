CC = gcc
CFLAGS = -ansi -std=c99 -Wall -pedantic
LIB = libpr.a

all: mpr sim_impr init_simulateurs print_demon
	
init_simulateurs: $(LIB) init_simulateurs.o sim_impr
	$(CC) $(LDFLAGS) init_simulateurs.o $(LIB) -o init_simulateurs
	
print_demon: $(LIB) print_demon.o
	$(CC) $(LDFLAGS) print_demon.o $(LIB) -o print_demon
	
mpr: $(LIB) mpr.o
	$(CC) $(LDFLAGS) mpr.o $(LIB) -o mpr

sim_impr: $(LIB) sim_impr.o
	$(CC) $(LDFLAGS) sim_impr.o $(LIB) -o sim_impr
	
init_simulateurs.o: init_simulateurs.c prlib.h
	$(CC) $(CFLAGS) -c -o init_simulateurs.o init_simulateurs.c

print_demon.o: print_demon.c prlib.h
	$(CC) $(CFLAGS) -c -o print_demon.o print_demon.c
	
mpr.o: mpr.c prlib.h
	$(CC) $(CFLAGS) -c -o mpr.o mpr.c
	
sim_impr.o: sim_impr.c prlib.h
	$(CC) $(CFLAGS) -c -o sim_impr.o sim_impr.c

$(LIB): fifo.o prlib.o
	ar -vq $(LIB) fifo.o prlib.o
	rm fifo.o prlib.o

prlib.o: prlib.c prlib.h fifo.h
	$(CC) $(CFLAGS) -c -o prlib.o prlib.c
	
fifo.o: fifo.c fifo.h
	$(CC) $(CFLAGS) -c -o fifo.o fifo.c
	
clean:
	
	rm *.o $(LIB) mpr sim_impr init_simulateurs print_demon
	
cleanall: clean
	rm \#*\# *~
	
cleanmpr:
	rm *.o $(LIB) mpr
	
cleansim_impr:
	rm *.o $(LIB) sim_impr

cleaninit_simulateurs:
	rm *.o $(LIB) init_simulateurs
	
cleanprint_demon:
	rm *.o $(LIB) print_demon
	