#include "prlib.h"

void usage();

int
main (int argc, char  *argv[])
{
	
	
	
	/***************
	* DECLARATIONS *
	***************/
	
	int ch, fd, l;
	char *cfile = NULL, lbuf[LINE_MAX+1], name[LINE_MAX], path[LINE_MAX], *args[SIM_CMD];
	pid_t pid;
	
	/*******************
	* FIN DECLARATIONS *
	*******************/
	
	
	
	/**********************
	* GESTION D'ARGUMENTS *
	**********************/
	
	if (argc != 3)
		usage();
	
	while ((ch = getopt( argc, argv, "+c:")) != -1)
		switch ( ch ){
			case 'c' :
				cfile = optarg;
				break;
			default:
			case '?' :
				usage();
				break;
		}
	
	/**************
	* FIN GESTION *
	**************/
	
	
	
	/**************************************
	* PARSAGE DU FICHIER DE CONFIGURATION *
	**************************************/
	
	if ((fd = open(cfile, O_RDONLY)) == -1)
		error("open: ");
	for (;;){
		l = getline(fd, lbuf);
		if (l == -1 )
			return -1;
		if (l == 0)
			return 0;
		*name = '\0';
		*path = '\0';
		sscanf(lbuf, "%s %s\n", name, path);
		smakea(name, path, args);
		if ((pid = fork()) == -1)
			error("fork: ");
		else if (pid == 0){
			execv(args[0], args);
			error("exec: ");
		}else
			printf("%s: PID %u\n", name, pid);
	}
	
	/**************
	* FIN PARSAGE *
	**************/
	
	
	
	return 0;
}

void
usage()
{
	/* Fonction pour afficher un message d'erreur lors de la mauvaise utilisation de la commande. */
	
	err("usage: init_simulateurs -c configuration_file");
}
