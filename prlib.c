#include "prlib.h"

/******************
* ERROR HANDLING *
******************/

void
error(const char *string)
{
	perror(string);
	exit(EXIT_FAILURE);
}

void
err(const char *string)
{
	fprintf(stderr, "%s\n", string);
	exit(EXIT_FAILURE);
}

void
warning(const char *string)
{
	perror(string);
}

void
warn(const char *string)
{
	fprintf(stderr, "%s\n", string);
}

/**********************
* FIN ERROR HANDLING *
**********************/



/******************
* MESSAGE HANLING *
******************/

/* La fonction read_msg tente de lire un message de type msg_t sur le descripteur de fichier fd,
retourne 0 s'il n'y a pas d'erreur, -1 sinon. */

int
read_msg(int fd, msg_t *msg)
{	
	char *buf, *ptr, *path;
	int r;
	/*********************
	* LECTURE DU MESSAGE *
	*********************/

	if ((r = (read(fd, (char *) &(msg->lng), sizeof(unsigned int)))) == -1)
		return -1;
	if (r == 0)
		return 0;
	
	if ((buf = (char *) malloc(msg->lng)) == NULL)
		return -1;
	
	if ((r = read(fd, buf, msg->lng)) == -1){
		free(buf);
		return -1;
	}
	
	/**************
	* FIN LECTURE *
	**************/
	
	
	
	/*********************
	* PARSAGE DU MESSAGE *
	*********************/
	
	ptr = buf;
	msg->type = *((char *) ptr);
	ptr += sizeof(char);
	
	msg->uid = *((uid_t *) ptr);
	ptr += sizeof(uid_t);
	
	msg->gid = *((gid_t *) ptr);
	ptr += sizeof(gid_t);
	
	msg->imprimante = ptr;
	msg->ref_abs = ptr;
	msg->ref_abs += strlen(msg->imprimante)+1;
	path = (char *) malloc(strlen(msg->ref_abs) + 1);
	strcpy(path, msg->ref_abs);
	msg->ref_abs = path;
	
	/**************
	* FIN PARSAGE *
	**************/
	
	/************
	* NETTOYAGE *
	************/
	ptr = buf;
	free(buf);
	buf=NULL;
	ptr = NULL;
	/****************
	* FIN NETTOYAGE *
	****************/
	
	return r;
}

/* La fonction write_msg écrit un message de type msg_t dans le descripteur de fichier fd,
retourne 0 s'il n'y a pas d'erreur, -1 sinon. */

int
write_msg(int fd, msg_t msg)
{
	char *buf, *ptr;
	int w;
	
	if ((buf = (char *) malloc(msg.lng + sizeof(unsigned int))) == NULL)
		error("malloc: ");
	
	ptr = buf;
	*((unsigned int *) ptr) = msg.lng;
	ptr += sizeof(unsigned int);
	*((char *)ptr) = msg.type;
	ptr += sizeof(char);
	*((uid_t *) ptr) = msg.uid;
	ptr += sizeof(uid_t);
	*((gid_t *) ptr) = msg.gid;
	ptr += sizeof(gid_t);
	memcpy(ptr, msg.imprimante, strlen(msg.imprimante)+1);
	ptr += (size_t) (strlen(msg.imprimante)+1);
	memcpy(ptr, msg.ref_abs, strlen(msg.ref_abs)+1);
	
	w = write(fd, buf, sizeof(unsigned int)+msg.lng);
	close(fd);
	
	free(buf);
	buf = NULL;
	ptr = NULL;
	
	if (w < 0)
		return -1;
	return w;
}

/* La fonction affiche un message de type msg_t sur la sortie standard. */
void
printm(msg_t msg)
{
	printf("%u, %c, %u, %u, %s %s\n", msg.lng, msg.type, msg.uid, msg.gid, msg.imprimante, msg.ref_abs);
}

/**************
* FIN MESSAGE *
**************/



/***************
* PR HANDLING *
***************/

pr_t *
new_pr(char *line)
{
	pr_t *impr;
	fifo_t *fifo;
	int tdes, m;
	char *name, path[LINE_MAX];
	
	if ((impr = malloc (sizeof (pr_t))) == NULL){
		perror ("new_fifo: ");
		exit(-1);
	}
	
	fifo = new_fifo();
	name = (char *) malloc(LINE_MAX +1);
	sscanf(line, "%s %s\n", name, path);
	
	if ((tdes = open(path, O_WRONLY)) == -1)
		error(path);
	
	if((m=fcntl(tdes, F_GETFL))<0)
		error("fcntl get: ");
	if( fcntl(tdes, F_SETFL, m | O_NONBLOCK)<0)
		error("fcntl set: ");
	
	impr->fifo = fifo;
	impr->name = name;
	impr->td_pr = tdes;
	impr->fdes = -1;
	
	return impr;
}

void
free_pr(pr_t *pr)
{
	free_fifo(pr->fifo, free);
	free(pr);
	pr = NULL;
}

/*********
* FIN PR *
*********/



/****************
* PATH HANDLING *
****************/

/* La fonction abs_path copie le chemin absolue de fname dans buf, elle retourne buf s'il n'y a pas d'erreur NULL sinon. */

char *
abs_path(char *fname, char *buf)
{
	if (*fname == '/')
		return strcpy(buf, fname);
	
	if (*fname == '~'){
		if(sprintf(buf, "%s%s", getenv("HOME"), fname+1) < 0)
			return NULL;
		return buf;
	}
	
	if (getcwd(buf, PATH_MAX)==NULL)
		return NULL;
	
	if(sprintf(buf, "%s/%s", buf, fname) < 0)
		return NULL;
	return buf;
}

/***********
* FIN PATH *
***********/



/**************
* LINE PARSER *
**************/

/* La fonction getline retourne la permière ligne à partir de la position courante du descripteur de fichier fd,
et la copie dans lbuf, avec le le caractère de retoure à la ligne et le caractère nulle à la fin */

int
getline(int fd, char *lbuf)
{
	size_t i, r, line = 0;
	char buf[LINE_MAX];
	for (;;){
		r = read(fd, buf, LINE_MAX);
		if (r == -1)
			return -1;
		if (r == 0)
			return line;
		else{
			for (i = 0; i < r; i++){
				if (buf[i] == '\n'){
					memcpy(lbuf + line, buf, i+1);
					lbuf[line + i + 1] = '\0';
					lseek(fd, -(r - i - 1), SEEK_CUR);
					return line + i + 1;
				}
			}
			memcpy(lbuf + line, buf, r);
			line += r;
		}		
	}
}

/******************
* FIN LINE PARSER *
******************/


/************
* BUF TOOLS *
************/


char *
bufchr(const char *buf, char c, size_t n)
{
	int i=0;
	for (;i < n; i++){
		if (buf[i] == c){
			return (char *) (buf + i);
		}
	}
	return NULL;
	
}

/* simulator make args */

void
smakea(char *name, char *path, char **args)
{
	char **arg;
	arg = args;
	*arg = "./sim_impr";
	arg++;
	*arg = "-t";
	arg++;
	*arg = path;
	arg++;
	*arg = "-n";
	arg++;
	*arg = name;
	arg++;
	*arg = NULL;
}

/* La fonction read_conf lis les informations necessaire à la creation d'imprimantes selon la structre pr_s,
et les stock dans un tableau passé en argument. */

int
read_conf(char *path, pr_t *printers[])
{
	int fdes, l, i = 0;
	char lbuf[LINE_MAX +1];
	
	if ((fdes = open(path, O_RDONLY)) == -1) 
		return -1;
	
	for(;;) {
		l = getline(fdes, lbuf);
		if (l == -1)
			return -1;
		if (l == 0)
			return i;
		printers[i] = new_pr(lbuf);
		i++;
	}
	
	close(fdes);
	
	return i;
}

/*****************
* FIN BUFF TOOLS *
*****************/



/***********************
* PERMISSIONS HANDLING *
***********************/

int
is_readable(char *path, uid_t uid, gid_t gid)
{
	struct stat sbuff;
	
	if (stat(path, &sbuff) == -1)
		return -1;
	if ((sbuff.st_uid == uid) && ((sbuff.st_mode & S_IRUSR) == S_IRUSR))
		return 1;
	else if ((sbuff.st_gid == gid) && ((sbuff.st_mode & S_IRGRP) == S_IRGRP))
		return 2;
	else if ((sbuff.st_uid != uid) && (sbuff.st_gid != gid) && ((sbuff.st_mode & S_IROTH) == S_IROTH))
		return 3;
	else
		return 0;
}

/*******************
* FIN PERSMISSIONS *
*******************/