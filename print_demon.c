#include "prlib.h"
#include "fifo.h"

void usage();

int
main(int argc, char *argv[])
{
	char *cmdfifo=NULL, *cfile=NULL, buf[MAX_CANON], *filename = NULL;
	int ch, cmddes, i, nb_printers, r;
	msg_t mbuff;
	pr_t *printers[PR_MAX];
		
	/**********************
	* GESTION D'ARGUMENTS *
	**********************/
	
	if (argc != 5)
		usage();
	
	while ((ch = getopt( argc, argv, "+t:c:")) != -1)
		switch ( ch ){
			case 't' :
				cmdfifo = optarg;
				break;
			case 'c' :
				cfile = optarg;
				break;
			default:
			case '?' :
				usage();
				break;
		}
	/**************
	* FIN GESTION *
	**************/
	/* Création du tube commande. */
	if (mkfifo(cmdfifo, S_IRUSR|S_IWUSR|S_IWGRP| S_IWOTH) == -1){
		if (errno == EEXIST)
			error("The daemon is already running.");
		error("mkfifo: ");
	}
	
	/* Ouvertur du tube commande en mode non bloquant. */
	if ((cmddes = open(cmdfifo, O_RDONLY | O_NONBLOCK)) == -1)
		error("open: ");
	
	/* Lecture du fichier de configuration et création des imprimantes. */
	if ( (nb_printers = read_conf(cfile, printers)) == -1)
		error("read_conf: ");
	
	
	
	for(;;){
		if (read_msg(cmddes, &mbuff) > 0){
			printf("Printing %s\n", basename(mbuff.ref_abs));
			if (mbuff.type == 'i')
				if (is_readable(mbuff.ref_abs, mbuff.uid, mbuff.gid) > 0){
					for (i = 0; i<nb_printers; i++){
						if (strcmp(mbuff.imprimante, printers[i]->name) == 0){
							fifo_add(printers[i]->fifo, mbuff.ref_abs);
							break;
						}
						if(i == nb_printers-1)
							fprintf(stderr, "%s: he printer %s is not configured, please add it to the configuration file", basename(argv[0]), mbuff.imprimante);
					}
				}
		}
		
		for(i = 0; i< nb_printers; i++){
			if ((printers[i]->fdes == -1) && !(fifo_empty(printers[i]->fifo))){
				filename  = (char *) fifo_top(printers[i]->fifo, filename);
				if ((printers[i]->fdes = open(filename, O_RDONLY)) == -1)
					error("open: ");
				free(filename);
				filename = NULL;
				if ((r = read(printers[i]->fdes, buf, MAX_CANON)) == -1)
					error("read: ");
				if (write(printers[i]->td_pr, buf, r) == -1){
					if(errno == EAGAIN){
						sleep(1);
						lseek(printers[i]->fdes, -r, SEEK_CUR);
						
						continue;
					}else{
						error("write");
					}
				}
			}else if (printers[i]->fdes > -1){
				if ((r = read(printers[i]->fdes, buf, MAX_CANON)) == -1)
					error("read: ");
				if(r == 0){
					if (write(printers[i]->td_pr, "\0\0\0\0\0\0\0\0\0\0", 10) == -1){
						if(errno == EAGAIN){
							sleep(1);
							continue;
						}else{
							error("write");
						}
					}
					close(printers[i]->fdes);
					printers[i]->fdes = -1;
				}else{
					if (write(printers[i]->td_pr, buf, r) == -1){
						if(errno == EAGAIN){
							sleep(1);
							lseek(printers[i]->fdes, -r, SEEK_CUR);
							continue;
						}else{
							error("write");
						}
					}
				}
			}
		}
	}
}



void
usage()
{
	/* Fonction pour afficher un message d'erreur lors de la mauvaise utilisation de la commande. */
	
	err("usage: print_demon -t tube_command -c configuration_file");
}
