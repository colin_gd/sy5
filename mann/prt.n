.\" Manpage for pr_t functions family
.\" Contact colinl.gd@gmail.com.
.Dd 31 December, 2013
.Dt PR n
.Sh NAME
.Nm pr
.Nd pr_t functions family.
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft pr_t *
.Fo new_pr
.Fa "char *line"
.Fc
.Ft void
.Fo free_pr
.Fa "pr_t *pr"
.Fc
.Sh DESCRIPTION
The pr function family allow to create and free the pr_t type.
The pr_s structure is defined as:
.Bd -literal
typedef struct pr_s
{
  fifo_t *fifo;
  char *name;
  int td_pr; /* tube descriptor printer */
} pr_t;
.Ed
.Pp
.Fn New_pr
creates a new pr_t from 
.Fa line
of the configuration file.
.Pp
.Fn Free_pr
frees
.Fa pr .
.Sh RETURN VALUES
For
.Fn New_pr
returns a new pr_t if succesful. Otherwise NULL is returned and 
.Fa errno
is set to indicate the error.
.Sh AUTHOR
Manual written by Colin Gonzalez (colinl.gd@gmail.com)
Sub-routines written by Cyrille Dakhlia (c.dakhlia@outlook.fr)