.\" Manpage for is_readable
.\" Contact colinl.gd@gmail.com.
.Dd 5 January, 2014
.Dt READABLE n
.Sh NAME
.Nm is_readable
.Nd states if a given user or group can read a file.
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft int
.Fo is_readable
.Fa "char *path"
.Fa "uid_t uid"
.Fa "gid_t gid"
.Fc
.Sh DESCRIPTION
.Fn Is_readable
states wether or not a file located at
.Fa path
is readable by a given
.Fa uid
or
.Fa gid .
.Sh RETURN VALUES
If the file is readable by the user the value 1 is returned, if it's readable by the group the value 2 is returned, and 3 is returned if it's readable by the others. Otherwise, a -1 is returned and the global variable 
.Fa errno 
is set to indicate the error.
.Sh AUTHOR
Colin Gonzalez (colinl.gd@gmail.com)