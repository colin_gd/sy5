.\" Manpage for bufchr
.\" Contact colinl.gd@gmail.com.
.Dd 16 December, 2013
.Dt BUFCHR n
.Sh NAME
.Nm bufchr
.Nd search a byte in allocated memory.
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft char *
.Fo bufchr
.Fa "char *buffer"
.Fa "char c"
.Fa "size_t n"
.Fc
.Sh DESCRIPTION
.Fn Bufchr
locates the first occurrence of
.Fa c
in the
.Fa buffer .
.Sh RETURN VALUES
.Fa Bufchr
returns a pointer to the byte located, or 
.Fa NULL
if no such byte exists within
.Fa n 
bytes.
.Sh AUTHOR
Colin Gonzalez (colinl.gd@gmail.com)