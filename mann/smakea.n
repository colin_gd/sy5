.\" Manpage for smakea
.\" Contact colinl.gd@gmail.com.
.Dd 16 December, 2013
.Dt SMAKEA n
.Sh NAME
.Nm smakea
.Nd builds a sim_impr command.
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft void
.Fo smakea
.Fa "char *name"
.Fa "char *path"
.Fa "char **args"
.Fc
.Sh DESCRIPTION
.Fn Smakea
builds a command array to execute with 
.Nm sim_impr
program using
.Fn exec
, with printer
.Fa name
using the fifo
.Fa path
and copies it to
.Fa args .
.Sh AUTHOR
Colin Gonzalez (colinl.gd@gmail.com)