.\" Manpage for error functions family
.\" Contact colinl.gd@gmail.com.
.Dd 16 December, 2013
.Dt ERRORS n
.Sh NAME
.Nm errors
.Nd errors functions family.
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft void
.Fo error
.Fa "const char *string"
.Fc
.Ft void
.Fo err
.Fa "const char *string"
.Fc
.Ft void
.Fo warning
.Fa "const char *string"
.Fc
.Ft void
.Fo warn
.Fa "const char *string"
.Fc
.Sh DESCRIPTION
The error handling functions allow to print error message and 
and exit with
.Fa EXIT_FAILURE
status.
.Pp
The warning functions allow to print messages on the standard error output.
.Pp
.Fn Error
and
.Fn Warning
print the corresponding error message to the global vriable
.Fa errno .
.Pp
.Fn Err
and
.Fn War
print a custom message.
.Sh AUTHOR
Colin Gonzalez (colinl.gd@gmail.com)