.\" Manpage for getline
.\" Contact colinl.gd@gmail.com.
.Dd 16 December, 2013
.Dt GETLINE n
.Sh NAME
.Nm getline
.Nd copies the next line in a buffer
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft int
.Fo getline
.Fa "int d"
.Fa "char *buffer"
.Fc
.Sh DESCRIPTION
.Fn Getline
copies the next line form the current position in file descriptor 
.Fa d
into the 
.Fa buffer ,
allocated with 
.Fa LINE_MAX+1 .
The line is delimited by the new line character.
.Sh RETURN VALUES
If succeseful, the number of bytes actually read is returned. Upon reading end-of-file, zero is returned. Otherwise, a -1 is returned and the global variable 
.Fa errno 
is set to indicate the error.
.Sh AUTHOR
Colin Gonzalez (colinl.gd@gmail.com)