.\" Manpage for read_conf
.\" Contact colinl.gd@gmail.com.
.Dd 31 December, 2013
.Dt READCONF n
.Sh NAME
.Nm read_conf
.Nd reads configuration file.
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft int
.Fo read_conf
.Fa "char *path"
.Fa "pr_t printers[]"
.Fc
.Sh DESCRIPTION
.Fn Getline
reads a configuration file located at
.Fa path
and creates pr_t and copies de pointer to the array
.Fa printers .
.Sh RETURN VALUES
If succeseful, the number of printers actually created is returned. Upon reading end-of-file, zero is returned. Otherwise, a -1 is returned and the global variable 
.Fa errno 
is set to indicate the error.
.Sh AUTHOR
Manual written by Colin Gonzalez (colinl.gd@gmail.com)
.Pp
Sub-routine wirtten by Cyrille Dakhlia (c.dakhlia@outlook.fr) and Colin Gonzalez