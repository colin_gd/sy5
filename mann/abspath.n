.\" Manpage for abs_path
.\" Contact colinl.gd@gmail.com.
.Dd 16 December, 2013
.Dt ABS_PATH n
.Sh NAME
.Nm abs_path
.Nd get the absolute path of a file or directory.
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft char *
.Fo abs_path
.Fa "char *fname"
.Fa "char *buffer"
.Fc
.Sh DESCRIPTION
.Fn Abs_path
copies the absolute path of
.Fa fname
into the 
.Fa buffer, 
previously allocated with 
.Fa PATH_MAX+1 .
.Sh RETURN VALUES
If succeseful, a pointer to the begining of the absolute path is returned. Otherwise, a 
.Fa NULL
pointer is returned and the global variable 
.Fa errno 
is set to indicate the error.
.Sh AUTHOR
Colin Gonzalez (colinl.gd@gmail.com)