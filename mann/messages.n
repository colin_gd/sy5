.\" Manpage for msg functions family
.\" Contact colinl.gd@gmail.com.
.Dd 16 December, 2013
.Dt MSG n
.Sh NAME
.Nm message
.Nd msg_t functions family.
.Sh LIBRARY
pr Library (prlib)
.Sh SYNOPSIS
.In prlib.h
.Ft int
.Fo read_msg
.Fa "int d"
.Fa "msg_t *buffer"
.Fc
.Ft int
.Fo write_msg
.Fa "int d"
.Fa "msg_t msg"
.Fc
.Ft void
.Fo printm
.Fa "msg_t msg"
.Fc
.Sh DESCRIPTION
The msg function family allow to read, write and print the msg_t type.
The msg_s structure is defined as:
.Bd -literal
typedef struct msg_s{
	unsigned int lng; /* size of the rest of the message */
	char type; /* the kind of command */
	uid_t uid; /* user-id of owner */
	gid_t gid; /* group-id of owner */
	char *imprimante; /* printer name */
	char *ref_abs; /* absolute path of the file to be printed */
};
.Ed
.Pp
.Fn Read_msg
read a 
.Fa msg_t
and copies it to the
.Fa buffer
from the the file descriptor
.Fa d .
.Pp
.Fn Write_msg
writes an
.Fa msg_t message
into the file descriptor
.Fa d
in binary format.
.Pp
.Fn Printm
prints an
.Fa msg_t
in string format on the standard output.
.Sh RETURN VALUES
For
.Fn Read_msg ,
if successful, the number of bytes actually read is returned.  Upon reading end-of-file, zero is returned. Otherwise, a -1 is returned and the global variable 
.Fa errno
is set to indicate the error.
.Pp
For 
.Fn Write_msg ,
upon successful completion the number of bytes which were written is returned.  Otherwise, a -1 is returned and the global variable
.Fa errno
is set to indicate the error.
.Sh AUTHOR
Colin Gonzalez (colinl.gd@gmail.com)